1. php app/console doctrine:database/create
2. php app/console doctrine:schema:update --force

REST API urls:

* /api      : get all orders JSON formated
* /api/{id} : get one order by id JSON formated

use ?yaml=true in the URL to get YAML formated