<?php

namespace Lg\TestBundle\XmlGetter;

use Symfony\Bridge\Monolog\Logger;

class LgXmlGetter
{
    private $url;
    private $logger;
    
    public function __construct($url, Logger $logger)
    {
        $this->url    = $url;
        $this->logger = $logger;
    }
    
    /**
     * Downloads the XML file from the URL and returns the feed
     * @throws Exception
     * @return string
     */
    public function getXml()
    {
        $xml = file_get_contents($this->url);
        
        if (!$xml) {
            $msg = "An error occured during file downloading.";
            
            $this->logger->info($msg);
            throw new \Exception($msg);    
        }
        
        $this->logger->info('Feed content : ' . $xml);        
        
        return $xml;
    }
}