<?php

namespace Lg\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        return $this->render('TestBundle:Index:index.html.twig');
    }
}
