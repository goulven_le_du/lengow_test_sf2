<?php

namespace Lg\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lg\TestBundle\Entity\Command;
use APY\DataGridBundle\Grid\Source\Vector;

class CommandController extends Controller
{
    /**
     * Downloads XML file and save datas into database
     */
    public function downloadAction()
    {
        // Download XML content
        $xml = $this->get("lg_test.lengow_test")->getXml();
        
        $em = $this->getDoctrine()->getManager();

        // Extract values and save in BD
        $commands = $this->parseXmlAndSave($xml, $em);
        
        $nbOrders = count($commands);
        
        if ($nbOrders > 0) {
            $msg = "$nbOrders new data(s) inserted in database";
        } else {
            $msg = "No data inserted because already exist in database";
        }
        
        $this->get('session')
             ->getFlashBag()
             ->add('success', $msg);
           
        return $this->displayAction();
    }
    
    /**
     * Displays stored orders in a grid
     */
    function displayAction()
    {
        // Get all commands
        $commands = $this->getDoctrine()
                         ->getManager()
                         ->getRepository('TestBundle:Command')
                         ->findAllArrayResult();
        
        if (empty($commands)) {
            return $this->render('TestBundle:Command:displayStoredDatas.html.twig', array("empty" => true));
        } else {        
            // Creates simple grid based on your entity (ORM)
            $source = new Vector($commands);
             
            // Get a grid instance
            $grid = $this->get('grid');
            
            // Attach the source to the grid
            $grid->setSource($source);
            
            // Configuration of the grid
            // ...
            
            // Manage the grid redirection, exports and the response of the controller
            return $grid->getGridResponse("TestBundle:Command:displayStoredDatas.html.twig");
        }
    }
    
    /**
     * Parses XML and stores datas into database
     * 
     * @param string $feed
     * @return array
     */
    private function parseXmlAndSave($feed, $em)
    {
        $commands = array();
                
        $xml = simplexml_load_string($feed);
        
        foreach ($xml->orders->order as $order) {
            // TODO : define validation rules and what to if not valid
            // Extract values from feed
            $order_id             = $order->order_id;
            $marketplace          = $order->marketplace;
            $idFlux               = $order->idFlux;
            $order_purchase_date  = new \DateTime($order->order_purchase_date);
            $order_purchase_heure = new \DateTime($order->order_purchase_heure);
            $order_amount         = (float)$order->order_amount;
            
            if ($this->alreadyInDb($order_id)) {
                // Do not insert in DB if already exists
                continue;
            }
            
            $command = new Command();
            
            $command->setOrderId($order_id)
                    ->setMarketplace($marketplace)
                    ->setIdFlux($idFlux)
                    ->setOrderPurchaseDate($order_purchase_date)
                    ->setOrderPurchaseHeure($order_purchase_heure)
                    ->setOrderAmout($order_amount);
            
            $em->persist($command);
            $commands[] = $command;
        }
        
        try {
            $em->flush();
        } catch (\Exception $e) {
            die(print_r($e->getMessage(), true));
        }
        
        return $commands;
    }
    
    /**
     * Returns TRUE if already exists in DB
     * @param string $order_id
     * @return boolean
     */
    private function alreadyInDb($order_id)
    {
        $result = $this->getDoctrine()
                       ->getManager()
                       ->getRepository('TestBundle:Command')
                       ->countByOrderId($order_id);
        
        return $result ? true : false;
    }
}
