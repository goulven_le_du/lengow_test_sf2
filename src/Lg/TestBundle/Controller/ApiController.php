<?php

namespace Lg\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    /**
     * Get all commands in JSON format by default or YAML format if $_GET[yaml]=true
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllCommandsAction()
    {
        $commands = $this->getDoctrine()
                         ->getManager()
                         ->getRepository('TestBundle:Command')
                         ->findAllArrayResult();
        
        if ($this->validGetYaml()) {
            $data = Yaml::dump($commands);
            $response = new Response($data);
        } else {
            $response = new JsonResponse();
            $response->setData($commands);            
        }
        
        return $response;
    }
    
    /**
     * Get one command by ID in JSON format by default or YAML format if $_GET[yaml]=true
     * 
     * @param string $id_order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCommandByIdAction($id_order)
    {
        $command = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('TestBundle:Command')
                        ->findByIdArrayResult($id_order);
        
        if (empty($command)) {
            throw $this->createNotFoundException('Unknown order id');
        }
        
        if ($this->validGetYaml()) {
            $data = Yaml::dump($command);
            $response = new Response($data);
        } else {
            $response = new JsonResponse();
            $response->setData($command);            
        }
        
        return $response;
    }
    
    /**
     * Check if yaml GET variable exists, returns its value if so. Returns FALSE otherwise
     * 
     * @return boolean
     */
    private function validGetYaml()
    {
        $yaml = $this->get('request')->get('yaml');
        
        if (isset($yaml) && $yaml == "true") {
            $result = true;
        } else {
            $result = false;
        }
        
        return $result;
    }
}
