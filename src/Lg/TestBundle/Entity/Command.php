<?php

namespace Lg\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lg\TestBundle\Entity\CommandRepository")
 * @UniqueEntity("orderId")
 */
class Command
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="orderId", type="string", length=255, unique=true)
     */
    private $order_id;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     */
    private $marketplace;

    /**
     * @var integer
     *
     * @ORM\Column(name="idFlux", type="integer")
     */
    private $idFlux;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_purchase_date", type="date", nullable=true)
     * @Assert\DateTime()
     */
    private $orderPurchaseDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_purchase_heure", type="time", nullable=true)
     * @Assert\DateTime()
     */
    private $orderPurchaseHeure;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amout", type="float")
     */
    private $orderAmout;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get order_id
     * 
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }
    
    /**
     * Get order_id
     *
     * @param string $id
     * @return Command
     */
    public function setOrderId($id)
    {
        $this->order_id = $id;
        
        return $this;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Command
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return Command
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set orderPurchaseDate
     *
     * @param \DateTime $orderPurchaseDate
     * @return Command
     */
    public function setOrderPurchaseDate($orderPurchaseDate)
    {
        $this->orderPurchaseDate = $orderPurchaseDate;

        return $this;
    }

    /**
     * Get orderPurchaseDate
     *
     * @return \DateTime 
     */
    public function getOrderPurchaseDate()
    {
        return $this->orderPurchaseDate;
    }

    /**
     * Set orderPurchaseHeure
     *
     * @param \DateTime $orderPurchaseHeure
     * @return Command
     */
    public function setOrderPurchaseHeure($orderPurchaseHeure)
    {
        $this->orderPurchaseHeure = $orderPurchaseHeure;

        return $this;
    }

    /**
     * Get orderPurchaseHeure
     *
     * @return \DateTime 
     */
    public function getOrderPurchaseHeure()
    {
        return $this->orderPurchaseHeure;
    }

    /**
     * Set orderAmout
     *
     * @param float $orderAmout
     * @return Command
     */
    public function setOrderAmout($orderAmout)
    {
        $this->orderAmout = $orderAmout;

        return $this;
    }

    /**
     * Get orderAmout
     *
     * @return float 
     */
    public function getOrderAmout()
    {
        return $this->orderAmout;
    }
}
